<?php

namespace OnNet\Reporting\Laravel\App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

// HTTP Exception Handlers
use ErrorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        ModelNotFoundException::class,
    ];


    protected $client = null

    protected $handler = null


    public function __construct(){

       $key =  Config::get('app.timezone', 'UTC');

       $monolog= Log::getMonolog();
       $app->configureMonologUsing(function($monolog = Log::getMonolog()) {
          $this->client = new Raven_Client('your dsn');

          $this->$handler = new Monolog\Handler\RavenHandler($client);
          $this->$handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));

          $monolog->pushHandler($this->$handler);
       });

       parent::__construct();
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     *
     * @return void
     */
    public function report(Exception $e)
    {


        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        return parent::render($request, $e);
    }
}
